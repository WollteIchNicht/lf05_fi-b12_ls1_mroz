﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       @SuppressWarnings("resource")
	Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       byte Anzahltickets;

       System.out.print("Zu zahlender Betrag (Euro):");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets: ");
       Anzahltickets = tastatur.nextByte();

       // Geldeinwurf
       // -----------
       
       eingezahlterGesamtbetrag = 0.00;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   
    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag * Anzahltickets+ "0 Euro"));
    	   System.out.print("Eingabe:");
    	   eingeworfeneMünze = tastatur.nextDouble();
    	   zuZahlenderBetrag = zuZahlenderBetrag + Anzahltickets - eingeworfeneMünze;
    	   
       //  eingezahlterGesamtbetrag += eingeworfeneMünze;
       //    zuZahlenderBetrag = zuZahlenderBetrag - eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 1.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 Euro");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 Euro");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}